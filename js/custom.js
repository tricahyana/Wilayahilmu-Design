var curent_scroll = 0;
$(window).scroll(function(){
	
	if ($(window).width() < 800) {
		if(document.body.scrollTop > curent_scroll){
			if(document.body.scrollTop > 60){
				$('.header').fadeOut();
			}
		}else{
			if(((curent_scroll - document.body.scrollTop) > 20) || (document.body.scrollTop < 50)){
				$('.header').fadeIn();
			}
		}
		
		curent_scroll = document.body.scrollTop;
	}else{
		if(document.body.scrollTop > 15){
			$("#subnavigation").addClass("subnavbar-crolldown");
			$("#socialmediasub-above").addClass("social-media-sub-above-active");
			$("#socialmediasub-above").removeClass("social-media-sub-above-non-active");
			
			$("#socialmediasub").addClass("social-media-sub-above-non-active");
			$("#socialmediasub").removeClass("social-media-sub-above-active");
		}else{
			$("#subnavigation").removeClass("subnavbar-crolldown");
			$("#socialmediasub-above").addClass("social-media-sub-above-non-active");
			$("#socialmediasub-above").removeClass("social-media-sub-above-active");
			
			$("#socialmediasub").addClass("social-media-sub-above-active");
			$("#socialmediasub").removeClass("social-media-sub-above-non-active");
		}	
	}
});

var sidebar_current_state_id = null;
var cat_id = null;
var path = null;
$(".sidebar-toggle").click(function(e) {
	e.preventDefault();
	
	/*
	  only for mobile.
		Close submenu dropdown.
	*/
	if ($(window).width() < 800) {
		$('#navbar-toggle2').trigger('click');
	}
	
	cat_id = $(this).data('subid');
    path = $(this).data('path');
	
	if(cat_id == sidebar_current_state_id){
		$("#wrapper").toggleClass("toggled");
		sidebar_current_state_id = null;
	}else if(sidebar_current_state_id == null){
		$("#wrapper").toggleClass("toggled");
		sidebar_current_state_id = cat_id;
	}else{
		sidebar_current_state_id = cat_id;
	}
	
	$("#sub-menu-header").text($(this).data('submenu'));
	
	$.post(path, {sub_menu: cat_id}, function(data) {
      $(".sidebar-nav").html(data);
	});
});

$("#close-sidebar").click(function(){
	$("#wrapper").toggleClass("toggled");
	sidebar_current_state_id = null;
});


