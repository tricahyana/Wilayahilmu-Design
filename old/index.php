<!DOCTYPE html>
<html>
    <head>
        <?php
        include 'page/head.php';
        ?>
    </head>
    <body>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-54474326-1', 'auto');
            ga('send', 'pageview');
        </script>

        <?php
        include 'page/header.php';
        ?>

        <div class="main container_12">
		
            <div class="grid_2">
                <div class="side-bar-left">
                    <div class="conten-title">
                        Popular
                    </div>
                    <?php
                    include 'page/popular.php';
                    ?>
                </div>
            </div>
            <div class="grid_8 alpha omega">
                <div class="main-center-conten">    
                    <div id="wowslider-container1">
                        <div class="ws_images">
                            <ul>
                                <?php
                                $i = 0;
                                foreach ($slidder as $result) {
                                    ?>
                                    <li><a href="<?= base_url("/read/" . $result->alias) ?>"><img src="<?= base_url($result->index_foto_konten) ?>" alt="< $result->judul_konten ?>" title="<?= $result->judul_konten ?>" id="wows1_<?= $i ?>"/></a></li>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </ul>
                        </div>

                        <div class="ws_bullets">
                            <div>
                                <?php
                                $i = 1;
                                foreach ($slidder as $result) {
                                    ?>
                                    <a href="#" title="<?= $result->judul_konten ?>"><?= $i ?></a>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </div>
                        </div>

                        <!--<span class="wsl"><a href="http://wowslider.com">jquery slider</a> by WOWSlider.com v6.3</span>-->
                        <div class="ws_shadow"></div>
                    </div>
                    <script type="text/javascript" src=<?= base_url("/source/js/libs/slider/engine1/wowslider.js") ?> ></script>
                    <script type="text/javascript" src=<?= base_url("/source/js/libs/slider/engine1/script-slidder.js") ?>></script>
                </div>	
            </div>
			<div class="grid_3">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- Sidebarwi -->
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-8654007416326536"
					 data-ad-slot="3565706203"
					 data-ad-format="auto"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
			
        </div>
			
        <div class="container_12 ">
		
            <div class="grid_2 hidden">Hidden</div>
            <div class="grid_8 omega alpha">
                <h3 class="header-accordion">Artikel Terkini</h3>
                <!--                    <div class="accordion-main">
                                        <div id="accordion" style="font-size: 13px;">
                <?php
//                            foreach ($accordion as $result) {
                ?>
                                                <h3> < $result->judul_konten . " - <span class='accordion-header'>" . date("d-M-Y H:i:s", strtotime($result->tgl_konten)) . "</span>" ?> </h3>
                                                <div>
                                                    <p>
                                                        <img class='conten-img' style='float:left;' width='170px' height='150px' src="//< base_url($result->index_foto_konten) ?>" />
                                                        < strip_tags(substr($result->konten, 0, 700)) ?>... 
                                                        <a href='//< base_url("/read/" . $result->id_konten) ?>' style='color:black'> (Read More)</a>
                                                    </p>
                                                </div>
                <?php
//                            }
                ?>
                                        </div>
                                    </div>-->

                <div class="simple-box-main">
                    <?php
                    foreach ($simple_box as $result) {
                        ?>
                        <a href='<?= base_url("/read/" . $result->alias) ?>'>
                            <div class='box'>
                                <div class='box-title'><?= substr($result->judul_konten, 0, 20) ?></div>
                                <div class='box-conten'><?= substr(strip_tags(str_replace('&nbsp;', '', $result->konten)), 0, 90) ?> ... </div>
                            </div>
                        </a>
                        <?php
                    }
                    ?>
                </div>

                <!--                    <div>
                                        <
                                        foreach ($large_box as $result) {
                                            ?>
                                            <a href="<base_url("/read/" . $result->id_konten) ?>">
                                                <div class="box-large">
                                                    <div class="box-large-title">< substr($result->judul_konten, 0, 50) ?></div>
                                                    <div class="box-large-conten">
                                                        <img class="conten-img" width="100px" height="100px" src="< $result->index_foto_konten ?>" style="float:left;">
                                                        < substr($result->konten, 0, 200) . "... " ?>
                                                    </div>
                                                </div>
                                            </a>
                                            <
                                        }
                                        ?>
                                    </div>-->


                <div class="bottom-conten">
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <?php
        include 'page/footer.php';
        ?>
    </div>
</body>
</html>
