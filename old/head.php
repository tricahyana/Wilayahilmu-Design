<title> <?php echo (isset($title))? $title . " - " : ""  ?> Wilayahilmu</title>
<meta name="google-site-verification" 
     content="Y5ssWQyTRxR4YkMw39y41MFVHV5OR-AKQB3rhSpaPUg" />
	 <!--new-->
	 <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	 <meta name="viewport" content="width=device-width, initial-scale=1">
     <!---->
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	 <meta name="description" content="Menampung segala ilmu pengetahuan agar semua orang dapat dengan mudah untuk menemukan apa yang ingin ia pelajari dan sekaligus meningkatkan minat dalam membaca suatu informasi yang dapat di jadikan sebuah ilmu yang bermanfaat.">
	 <meta name="keywords" content="tutorial, wilayahilmu, nurhaman, kasyfil aziz tricahyana, ilmu pengetahuan, ilmu, pengetahuan, mahasiswa, pelajar, wilayah ilmu, belajar, guru, dosen, sejarah, bagaimana, how, cara, menggunakan, mencoba, pengalaman, buku, buku digital, e-learning, keyword">
	 <link rel="shortcut icon" href="<?= base_url("/source/img/wi.png") ?>">
	 <meta name="author" content="Wilayahilmu, Kasyfil Aziz Tricahyana, Nurhaman" />
	 <link rel=”author” href=”http://wilayahilmu.com”/>
	 <link rel=”publisher” href=”http://wilayahilmu.com”/>
	 <script type="text/javascript" src=<?= base_url("/source/js/libs/slider/engine1/jquery.js") ?>></script>
	 <script type="text/javascript" src=<?= base_url("/source/js/libs/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js") ?>></script>
<!--
<script type="text/javascript" src=< base_url("/source/js/libs/speech/speech.min.js") ?>></script>
<script type="text/javascript" src=< base_url("/source/js/libs/speech/annyang.min.js") ?>></script>
-->

<script>
    var ADAPT_CONFIG = {
        path: '<?= base_url("/source/js/libs/adapt/css/") ?>/',
        dynamic: 'true',
        range: [
            '0px    to 980px  = mobile.css',
            '980px  to 1360px = 960.css',
            '1360px           = 1366.css'
            //'1600px to 1920px = 1560.css',
            //'1940px to 2540px = 1920.css',
            //'2540px           = 2520.css'
        ]
    };
</script>
<script type="text/javascript" src="<?= base_url("/source/js/libs/adapt/js/adapt.min.js"); ?>"></script>
<link type="text/css" rel="stylesheet" href=<?= base_url("/source/css/index.css") ?>>

<!-- Facebook Pixel Code -->
<script>
// ViewContent
// Track key page views (ex: product page, landing page or article)
fbq('wilayahilmu.com', 'wilayahilmu.com');

// Search
// Track searches on your website (ex. product searches)
fbq('wilayahilmu.com', 'wilayahilmu.com');

// CompleteRegistration
// Track when a registration form is completed (ex. complete subscription, sign up for a service)
fbq('wilayahilmu.com', 'wilayahilmu.com');
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1642711282669712');
fbq('wilayahilmu.com', "wilayahilmu.com");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1642711282669712&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->