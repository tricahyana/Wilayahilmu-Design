<div>
    <link type="text/css" rel="stylesheet" href=<?= base_url("/source/js/libs/jquery-ui-1.10.4.custom/css/custom-theme/jquery-ui-1.10.4.custom.min.css") ?>>
    <link rel="stylesheet" type="text/css" href="<?= base_url("/source/admin/css/control.css") ?>">
    <link rel="stylesheet" type="text/css" href=<?= base_url("/source/js/libs/slider/engine1/style.css") ?> />
    <script type="text/javascript" src=<?= base_url("/source/js/script.js") ?>></script>
</div>

<div class = "footer" id = "footer"><!--Tempat Untuk Footer -->
    <div class = "footer-grey">
        
    </div>
    <div class = "footer-botom">
        <div class="footerkiribawah">
            <h3><a href="http://wilayahilmu.com/read/14" target="_blank">About</a></h3>
            <h5>Learn Everywhere</h5>
            <div class="penjelasan">
                <li>Bandung, Jawa Barat</li>
                <li>Email : <a href="mailto:support@wilayahilmu.com">support@wilayahilmu.com</a></li>
                <li>Facebook : <a href="https://www.facebook.com/wilayahilmu/" target="_blank">Wilayahilmu</a></li>
                <!--<li>Twitter : <a href="https://twitter.com/wilayahilmu/" target="_blank">Wilayahilmu</a></li>-->
            </div>
        </div>
    
        <!--        <div class="footerkiritengah">
                    <h3>Contributor</h3>
                    <h5>Contoh Text Saya</h5>
                    <div class="penjelasan">
                        <li>Contoh Text</li>
                        <li>Contoh : <a href="mailto:support@wilayahilmu.com">Text</a></li>
                        <li>Facebook : <a href="http:www.facebook.com/wilayahilmu/" target="blank">Wilayahilmu</a></li>
                    </div>
                </div>
                <div class="footertengah">
                    <h3>Link</h3>
                    <div class="penjelasanlink">
                        <li><a href="#">Contoh Text</a></li>
                        <li><a href="#">Text</a></li>
                        <li><a href="http:www.facebook.com/wilayahilmu/" target="blank">Wilayahilmu</a></li>
                    </div>
                </div>-->
    </div>
    <div class = "footer-top">
        Copyright Wilayahilmu 2014
    </div>
</div>
