<?php
if (@!$search) {
    ?>
    <script src="<?= base_url("source/js/side-bar.js") ?>"></script>
    <span id="filter-span">Filter +</span><br>
    <div id='side-bar-filter'>
      <input type="text" name="date_1" id="filter-date-1" placeholder="Tgl Mulai" size="14" value="<?= @$date_1 ?>"> s.d
      <input type="text" name="date_2" id="filter-date-2" placeholder="Tgl Akhir" size="14" value="<?= @$date_2 ?>">
      <select name="kategori" id="filter-kategori" >
          <option value="all">Filter Kategori (All)</option>
          <?php
          foreach ($kategori as $result) {
              if ($result->id_kategori == $id_kategori) {
                  ?>
                    <option value="<?= $result->id_kategori ?>" selected="selected"><?= $result->kategori ?></option>
                  <?php
              } else {
                  ?>
                    <option value="<?= $result->id_kategori ?>"><?= $result->kategori ?></option>
                  <?php
              }
          }
          ?>
      </select>
      <input type="submit" value="OK" id="filter-submit" onclick="filterSidebar('<?= @$sub_menu ?>', '<?= base_url("/konten/list_konten") ?>', document.getElementById('filter-date-1').value, document.getElementById('filter-date-2').value, document.getElementById('filter-kategori').value)">
    </div>
    <?php
}

if (is_array($data)) {
    ?> <h3> <?= $header ?> </h3> <?php
    foreach ($data as $result) {
      ?>
        <div class='list-content'><a href='<?= base_url("/read/" . $result->alias) ?>'>
        <?= $result->judul_konten ?> - <span><?= '(' . $result->kategori . ') '  . date('d-M-Y H:i:s', strtotime($result->tgl_konten)) ?></span>
        </a><br></div>
      <?php
    }
} else {
    echo $data;
}
?>

