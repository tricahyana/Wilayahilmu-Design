<header ><!--Tempat Untuk Header -->
    <div class="header" id="header">
        <h1 class="header-menu">
            <a class="logo-header" href='http://www.wilayahilmu.com/'><img src="http://wilayahilmu.com/source/img/logo_white_57.png" alt="Wilayah Ilmu" width="100"></a>
            
            <?php
            foreach ($menu as $result) {                
                ?><a href='<?= base_url("home/" . $result->alias)?>'><div <?= ($result->id_menu == $this->session->userdata('menu')?"class='selected-menu'":'') ?>><?= $result->nama_menu ?> </div></a><?php
            }
            ?>
            <a href='http://wilayahilmu.com/read/wilayahilmu-about'><div>About</div></a>
        </h1>

        <div class="header-contact">
            <input type="search" placeholder="Cari . . " size = "30" id="cari_text"><button id="cari">Go</button>
            <input type="hidden" value="<?= base_url("/konten/search/") ?>/" id="base_url">
        </div>
		<div class="header-contact">
            <a href="https://www.facebook.com/wilayahilmu" target="blank"><button class="socialmediaicon facebook">f</button></a>
			<a href="https://twitter.com/wilayahilmuinfo" target="blank"><button class="socialmediaicon twitter">t</button></a>
        </div>
    </div>
    <div class="sub-header-menu">
        <?php
        foreach ($sub_menu as $result) {
            ?>
            <div id = 'sub-menu_<?= $result->id_sub_menu ?>' class = 'sub-menu-list' sub_menu ="<?= $result->id_sub_menu ?>"><?= $result->sub_menu ?></div>
            <script>
                //Side menu effect
                $("#sub-menu_<?= $result->id_sub_menu ?>").click(function(event) {
                    openSidebar('<?= $result->id_sub_menu ?>', '<?= base_url('konten/list_konten/') ?>');
                });
            </script>
            <?php
        }
        ?>
    </div>

    <div class="side-header-menu">
        <div class="close-side-bar">X</div>
        <div class="side-header-menu-list">Harap Tunggu .....</div>
    </div>

    <div class="modal"></div>

    <div class="sub-header" id="sub-header"><!-- Tempat Untuk Sub Header dan Logo -->
        

        <!--<div class="logo">
            <img src="<?= base_url("/source/img/logo_white_57.png") ?>" alt="Wilayah Ilmu" width="200">
        </div>-->
    </div>
</header>
	